package protonbeam;

public class coulombScatter {
    double rho; double Z; double A;
    double materialThickness;

        public coulombScatter(double rho_in, double Z_in, double A_in)
    {
        rho = rho_in;
        Z = Z_in;
        A = A_in;
    }

    public double getX0()
    {
        double X0_n = 716.4*A;
        double X0_d = rho*Z*(Z+1)*Math.log(287/Math.sqrt(Z));
        double X0 = X0_n/X0_d;
        // shall return X0 in m
        return X0/100; // /100 converts to m
    }

    public double getTheta0(Particle p, double x)
    {
        materialThickness = x/getX0(); // material thickness in radiation lengths
        double theta0_part_1 = 13.6/(p.beta()*p.momentum());
        double theta0_part_2 = p.Q*Math.sqrt(materialThickness);
        double theta0_part_3 = 1+0.038*Math.log(materialThickness);
        double theta0 = theta0_part_1*theta0_part_2*theta0_part_3;
        // shall return Theta0 for material thickness x
        return theta0;
    }
}

package protonbeam;
import java.util.Random;
class Geometry
{   
    private Random randGen;
    private static final int maxShapes = 100;
    private int nshapes;
    private int [] type;
    private double [] rho;
    private double [] Z;
    private double [] A;
    private double radius;

    private double [][] shapes;
    
    private energyLoss [] Eloss;
    private coulombScatter [] MultScatter;

    private double minfeaturesize;

    public Geometry(double featuresize)
    {
        randGen = new Random();
        nshapes = 0;
        type = new int[maxShapes];
        rho = new double[maxShapes];
        Z = new double[maxShapes];
        A = new double[maxShapes];
        shapes = new double[maxShapes][];
        
        Eloss = new energyLoss[maxShapes];
        MultScatter = new coulombScatter[maxShapes];

        minfeaturesize = featuresize;
    }

    public int getNshapes() { return nshapes; }

    public int AddCuboid(double x0, double y0, double z0,
                         double x1, double y1, double z1,
                         double rhoin, double Zin, double Ain)
    {
        if (nshapes >= maxShapes) {
            return -1;
        }
        
        type[nshapes] = 1;
        shapes[nshapes] = new double[6];
        shapes[nshapes][0] = x0;
        shapes[nshapes][1] = y0;
        shapes[nshapes][2] = z0;
        shapes[nshapes][3] = x1;
        shapes[nshapes][4] = y1;
        shapes[nshapes][5] = z1;

        rho[nshapes] = rhoin;
        Z[nshapes] = Zin;
        A[nshapes] = Ain;

        
        Eloss[nshapes] = new energyLoss(rhoin, Zin, Ain);
        MultScatter[nshapes] = new coulombScatter(rhoin, Zin, Ain);

        nshapes++;
        return (nshapes-1);
    }
    
    public int AddSphere(double xC, double yC, double zC,
                         double radius, double rhoin, double Zin, double Ain)
    {
        if (nshapes >= maxShapes) {
            return -1;
        }
        
        type[nshapes] = 2;
        shapes[nshapes] = new double[6];
        shapes[nshapes][0] = xC;
        shapes[nshapes][1] = yC;
        shapes[nshapes][2] = zC;
        shapes[nshapes][3] = radius;
        
        rho[nshapes] = rhoin;
        Z[nshapes] = Zin;
        A[nshapes] = Ain;

        Eloss[nshapes] = new energyLoss(rhoin, Zin, Ain);
        MultScatter[nshapes] = new coulombScatter(rhoin, Zin, Ain);

        nshapes++;
        return (nshapes-1);
    }
    
    public int AddWheel(double x0, double y0, 

                        double z0, double z1, 

                        double WheelRad,double SpanAngle, double inSpanAngle, 

                        double rhoin, double Zin, double Ain)  

    {  
        if (nshapes >= maxShapes) {  

            return -1;  

        }  
        type[nshapes] = 3;  
        shapes[nshapes] = new double[7]; 

        shapes[nshapes][0] = x0; 

        shapes[nshapes][1] = y0; 

        shapes[nshapes][2] = z0; 

        shapes[nshapes][3] = z1; 

        shapes[nshapes][4] = WheelRad; 

        shapes[nshapes][5] = SpanAngle; 

        shapes[nshapes][6] = inSpanAngle; 
        
        rho[nshapes] = rhoin;  
        Z[nshapes] = Zin;  
        A[nshapes] = Ain; 

        Eloss[nshapes] = new energyLoss(rhoin, Zin, Ain);  
        MultScatter[nshapes] = new coulombScatter(rhoin, Zin, Ain);  

        nshapes++;  
        return (nshapes-1);  

    }  
    
    public void Print()
    {
        System.out.println("stored " + getNshapes() + " objects.");
        for (int i = 0; i < nshapes; i++) {
            if (i == 0) {
                System.out.println("Maximum size of experiment given by object 0:");
            }
            if (type[i] == 1) {
                System.out.println("Geometry object #" + i + " = cuboid.");
                System.out.printf("   corners (%f, %f, %f) - (%f, %f, %f)%n",
                                  shapes[i][0], shapes[i][1], shapes[i][2],
                                  shapes[i][3], shapes[i][4], shapes[i][5]);
            }
            System.out.printf("   material rho = %.3f g/cm^3, Z = %f, A = %f%n",
                              rho[i], Z[i], A[i]);
        }
        System.out.println("When scanning for volume transitions, the smallest feature size discovered will be " + minfeaturesize + "m.");
    }

    public boolean isInVolume(double x, double y, double z, int id)
    {
        // test if point (x,y,z) is in volume with identifier id
        
        // abort if being asked for an undefined volume
        if (id >= getNshapes()) {
            return false;
        }
        
        if (type[id] == 1) {
            // cuboid
            return ( shapes[id][0] <= x
                     && shapes[id][1] <= y
                     && shapes[id][2] <= z
                     && x <= shapes[id][3]
                     && y <= shapes[id][4]
                     && z <= shapes[id][5] );
        }
        
        if (type[id] == 2) {
            //sphere
            double maxAngle = 2.0*Math.PI/180;
            for (int angle = 0; angle<maxAngle; angle++){ // rewrite
                double pos = shapes[id][3]/Math.cos(angle); // shapes[id][3] is radius
                return (( shapes[id][0]+pos) <= x // xC centre in x axis (xC less than x)
                       && ( shapes[id][1]+pos) <= y //yC centre in y axis 
                       && ( shapes[id][2]+pos) <= z // zC centre in z axis
                       && x <= (shapes[id][0]-pos)
                       && y <= (shapes[id][1]-pos)
                       && z <= (shapes[id][2]-pos) );
            }
        }
        
        if (type[id] == 3) {  
        double angles = shapes[id][5]*Math.PI/180.;
        double inwheelang = shapes[id][6]*Math.PI/180.;
        for (int startang = 0; startang <= angles; startang++){ 
        //double xwheel = shapes[nshapes][5]*Math.cos(inwheelang); 
        //double ywheel = shapes[nshapes][5]*Math.sin(inwheelang); 
        return ( shapes[id][4]*Math.cos(inwheelang)+shapes[id][0] <= x  
                && shapes[id][4]*Math.sin(inwheelang)+shapes[id][1] <= y  
                && shapes[id][2] <= z  
                && z <= shapes[id][3] );  
        } 
        }  
        return false;
   } 
    public int getVolume(double x, double y, double z)
    {
        // cycle through volumes in opposite order
        for (int i = getNshapes()-1; i >= 0; i--) {
            if (isInVolume(x, y, z, i)) {
                return i;
            }
        }
        
        // if we arrived here, we are outside everything
        return -1;
    }

    public boolean isInVolume(Particle p, int id)
    {
        // test if particle p is currently in volume with identifier id
        return isInVolume(p.x, p.y, p.z, id);
    }
    
    public int getVolume(Particle p)
    {
        // get the highest volume number the particle is in
        return getVolume(p.x, p.x, p.z);
    }
    
    public void doEloss(Particle p, double dist)
    {
        int volume = getVolume(p);
        
        if (volume >= 0) {
            double lostE = Eloss[volume].getEnergyLoss(p)*dist;
            p.reduceEnergy(lostE);
        }
    }
    
    public void doMultScatter(Particle p, double dist)
    {
        int volume = getVolume(p);
        
        if (volume < 0) {
            return;
        }
        
        double theta0 = MultScatter[volume].getTheta0(p, dist);

        if (theta0 > 0.) {
            p.applySmallRotation(randGen.nextGaussian()*theta0,
                                 randGen.nextGaussian()*theta0);
        }
        
    }

    public double [][] detectParticles(Track simtrack)
    {
        double [][] detection = new double[getNshapes()][4];

        // loop over each volume and average over the matching points
        for (int idet = 1; idet < getNshapes(); idet++) {

            // count points in volume
            int ncross = 0;

            for (int ipoint = 0; ipoint < simtrack.lastpos; ipoint++) {
                if (getVolume(simtrack.x[ipoint], simtrack.y[ipoint], simtrack.z[ipoint]) == idet) {
                    detection[idet][0] += simtrack.t[ipoint];
                    detection[idet][1] += simtrack.x[ipoint];
                    detection[idet][2] += simtrack.y[ipoint];
                    detection[idet][3] += simtrack.z[ipoint];
                    ncross++;
                }
            }
            if (ncross > 0) {
                for (int i = 0; i < 4; i++) {
                    detection[idet][i] /= ncross;
                }
            }
        }
        
        return detection;
    }

    public double scanVolumeChange(Particle p, Particle pold, int lastVolume)
    {
        // scan the straight line between last and new position, if we missed a
        // feature of the experiment
        double dist = p.distance(pold);
        int nsteps = (int) (dist/minfeaturesize) + 1;

        if (nsteps <= 1) {
            // step was small enough, continue
            return 1.;
        }
        
        double [] pos = {pold.x, pold.y, pold.z};
        double [] end = {p.x, p.y, p.z};
        double [] delta = new double[3];
        for (int i = 0; i < 3; i++) {
            delta[i] = (end[i]-pos[i])/nsteps;
        }
        for (int n = 1; n <= nsteps; n++) {
            if (getVolume(pos[0]+n*delta[0], pos[1]+n*delta[1], pos[2]+n*delta[2]) != lastVolume) {
                if (n == 1) {
                    return (0.5 / nsteps);
                } else {
                    return (n-1.0) / nsteps;
                }
            }
        }
        return 1.;
    }
}
package protonbeam;

public class energyLoss {
    double K = 47; //MeV/cm^2
    double m_e = 0.511; //MeV
    double rho; double Z; double A;
    double dE;
    
    public energyLoss(double rho_in, double Z_in, double A_in)
    {
        rho = rho_in;
        Z = Z_in;
        A = A_in;
    }
    
    public double getEnergyLoss(Particle p)
    {
         // shall return energy loss in MeV/m
        double W_max_n = 2*m_e*p.beta()*p.beta()*p.gamma()*p.gamma();
        double W_max_d = (1+(2*p.gamma()*m_e/p.m))+((m_e/p.m)*(m_e/p.m));
        double W_max = W_max_n/W_max_d; // max E transfer in single collison
        double I = 0.0000135*Z; // mean excitation energy
        double dE_part_1 = K*p.Q*p.Q*rho*(Z/A)*(1/(p.beta()*p.beta()));
        double dE_part_2 = Math.log((2*m_e*p.beta()*p.beta()*p.gamma()*p.gamma()*W_max)/(I*I));
        double dE_part_3 = (0.5*dE_part_2)-(p.beta()*p.beta());
        if (rho ==0 || Z==0 || A==0){
            dE= 0.;
            }
        else {
            dE = dE_part_1*dE_part_3;
        }
        return dE*100; //*100 converts MeV/cm to MeV/m
    }
}
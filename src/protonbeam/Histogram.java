package protonbeam;

import java.io.IOException;
import java.io.PrintWriter;

class Histogram
{       
    private double binlow, binlowX, binlowY, binhigh, binhighX, binhighY;
    private double binwidth, binwidthX, binwidthY;
    private int nbins, nbinsX, nbinsY;
    private double[] binCentre, binCentreX, binCentreY;
    private String histname;

    // double array to store the actual histogram data
    private double[] sumWeights, sumWeightsX, sumWeightsY;

    private long underflow, underflowX, underflowY, overflow, overflowX, overflowY;
    private long nfilled, nfilledX, nfilledY;

    // constructor for a 1D Histogram
    public Histogram(int numberOfBins, double start, double end, String name)
    {
        // store the parameters and setup the histogram
        // note that parameters need to have different names than class variables
        nbins = numberOfBins;
        binlow = start;
        binhigh = end;
        histname = name;

        binwidth = (binhigh - binlow) / (double) nbins;
        sumWeights = new double[nbins];
        underflow = 0;
        overflow = 0;
        nfilled = 0;
        
        // calculate and save the x coordinate of the centre of each bin
        binCentre = new double[nbins];
        for (int i = 0; i < nbins; i++) {
            binCentre[i] = binlow + (i+0.5)*binwidth;
        }
    } // end of constructor for a 1D bin
    
    // constructor for a 2D Histogram
    public Histogram(int numberOfBinsX, int numberOfBinsY, double startX, double startY,
            double endX, double endY, String name)
    {
        // store the parameters and setup the histogram
        // note that parameters need to have different names than class variables
        nbinsX = numberOfBinsX;
        nbinsY = numberOfBinsY;
        binlowX = startX;
        binlowY = startY;
        binhighX = endX;
        binhighY = endY;
        histname = name;

        binwidthX = (binhighX - binlowX) / (double) nbinsX;
        binwidthY = (binhighY - binlowY) / (double) nbinsY;
        sumWeightsX = new double[nbinsX];
        sumWeightsY = new double[nbinsY];
        underflowX = 0; underflowY = 0;
        overflowX = 0; overflowY = 0;
        nfilledX = 0; nfilledY = 0;
        
        // calculate and save the x coordinate of the centre of each bin
        binCentreX = new double[nbinsX];
        for (int i = 0; i < nbinsX; i++) {
            binCentreX[i] = binlowX + (i+0.5)*binwidthX;
        }
        // calculate and save the y coordinate of the centre of each bin
        binCentreY = new double[nbinsY];
        for (int i = 0; i < nbinsY; i++) {
            binCentreY[i] = binlowY + (i+0.5)*binwidthY;
        }
    } // end constructor for 2D bin
    
    // getNbins() for 1D histogram
    public int getNbins()
    {
        return nbins;
    }
    // getNbins() for 2D histogram
    public int getNbinsX()
    {
        return nbinsX;
    }
    public int getNbinsY()
    {
        return nbinsY;
    }

    // getUnderflow() for 1D histogram
    public long getUnderflow()
    {
        return underflow;
    }
    // getUnderflow() for 2D histogram
    public long getUnderflowX()
    {
        return underflowX;
    }
    public long getUnderflowY()
    {
        return underflowY;
    }

    // getOverrflow() for 1D histogram
    public long getOverflow()
    {
        return overflow;
    }
    // getOverrflow() for 2D histogram
    public long getOverflowX()
    {
        return overflow;
    }
    public long getOverflowY()
    {
        return overflow;
    }
    
    // getNfilled() for 1D histogram
    public long getNfilled()
    {
        return nfilled;
    }
    // getNfilled() for 2D histogram
    public long getNfilledX()
    {
        return nfilledX;
    }
    public long getNfilledY()
    {
        return nfilledY;
    }

    // method for 1D histogram
    public void fill(double value)
    {
        if (value < binlow) {
            underflow++;
        } else if (value >= binhigh) {
            overflow++;
        } else {
            // add weight to the correct bin
            int ibin = (int) ( (value - binlow)/binwidth);
            sumWeights[ibin] = sumWeights[ibin] + 1.0;
        }
        nfilled++;
    }
    
    // method for 2D hist FINISH
    public void fill(double valueX, double valueY)
    {
        if (valueX < binlowX) {
            underflowX++;
        } else if (valueX >= binhighX) {
            overflowX++;
        } else {
            // add weight to the correct bin
            int ibinX = (int) ( (valueX - binlowX)/binwidthX);
            sumWeightsX[ibinX] = sumWeightsX[ibinX] + 1.0;
        }
        nfilledX++;
        
        if (valueY < binlowY) {
            underflowY++;
        } else if (valueX >= binhighY) {
            overflowY++;
        } else {
            // add weight to the correct bin
            int ibinY = (int) ( (valueY - binlowY)/binwidthY);
            sumWeightsY[ibinY] = sumWeightsY[ibinY] + 1.0;
        }
        nfilledY++;
    } // method end

    // getContent() for 1D histogram
    public double getContent(int nbin)
    {
        // returns the contents on bin 'nbin' to the user
        return sumWeights[nbin];
    }
    // getContent() for 2D histogram
    public double getContentX(int nbinX)
    {
        // returns the contents on bin 'nbin' to the user
        return sumWeightsX[nbinX];
    }
    public double getContentY(int nbinY)
    {
        // returns the contents on bin 'nbin' to the user
        return sumWeightsY[nbinY];
    }

    // getError() for 1D histogram
    public double getError(int nbin)
    {
        // returns the error on bin 'nbin' to the user
        return Math.sqrt(sumWeights[nbin]);
    }
    // getError() for 1D histogram
    public double getErrorX(int nbinX)
    {
        // returns the error on bin 'nbin' to the user
        return Math.sqrt(sumWeightsX[nbinX]);
    }
    public double getErrorY(int nbinY)
    {
        // returns the error on bin 'nbin' to the user
        return Math.sqrt(sumWeightsY[nbinY]);
    }
    
    //-------------------------------------
    // print for 1D histogram
    public void print()
    {
        for (int bin = 0; bin < getNbins(); bin++) {
            System.out.println("Bin " + bin + " = " +getContent(bin)
                               + " +- " + getError(bin));
        }
        System.out.println("The number of fills = " + getNfilled());
        System.out.println("Underflow = " + getUnderflow()
                           + ", Overflow = " + getOverflow());
    }
    // print for 2D histogram
    public void printX()
    {
        for (int bin = 0; bin < getNbinsX(); bin++) {
            System.out.println("Bin " + bin + " = " +getContentX(bin)
                               + " +- " + getErrorX(bin));
        }
        System.out.println("The number of fills = " + getNfilledX());
        System.out.println("Underflow = " + getUnderflowX()
                           + ", Overflow = " + getOverflowX());
    }
    public void printY()
    {
        for (int bin = 0; bin < getNbinsY(); bin++) {
            System.out.println("Bin " + bin + " = " +getContentY(bin)
                               + " +- " + getErrorY(bin));
        }
        System.out.println("The number of fills = " + getNfilledY());
        System.out.println("Underflow = " + getUnderflowY()
                           + ", Overflow = " + getOverflowY());
    }
    
    //-------------------------------------
    // writeToDisk for 1D histogram
    public void writeToDisk(String filename)
    {
        // this sends the output to a file with name "filename"
        // the block with try { ... } catch (IOException e) { ... } is needed to handle the case,
        // where opening the file fails, e.g. disk is full or similar
        PrintWriter outputFile;
        try {
            outputFile = new PrintWriter(filename);
        } catch (IOException e) {
            System.err.println("Failed to open file " + filename + ". Histogram data was not saved.");
            return;
        }

        // Write the file as a comma seperated file (.csv) so it can be read it into EXCEL
        // first some general information about the histogram
        outputFile.println("histname, " + histname);
        outputFile.println("binlow, " + binlow);
        outputFile.println("binwidth, " + binwidth);
        outputFile.println("nbins, " + nbins);
        outputFile.println("underflow, " + underflow);
        outputFile.println("overflow, " + overflow);

        // now make a loop to write the contents of each bin to disk, one number at a time
        // together with the x-coordinate of the centre of each bin.
        for (int n = 0; n < nbins; n++) {
            // comma separated values
            outputFile.println(n + "," + binCentre[n] + "," + getContent(n) + "," + getError(n));
        }
        outputFile.close(); // close the output file
    }
    // writeToDisk for 2D histogram
    public void writeToDisk2D(String filename)
    {
        // this sends the output to a file with name "filename"
        // the block with try { ... } catch (IOException e) { ... } is needed to handle the case,
        // where opening the file fails, e.g. disk is full or similar
        PrintWriter outputFile;
        try {
            outputFile = new PrintWriter(filename);
        } catch (IOException e) {
            System.err.println("Failed to open file " + filename + ". Histogram data was not saved.");
            return;
        }

        // Write the file as a comma seperated file (.csv) so it can be read it into EXCEL
        // first some general information about the histogram
        outputFile.println("histname, " + histname);
        outputFile.println("binlowX, " + binlowX);
        outputFile.println("binwidthX, " + binwidthX);
        outputFile.println("nbinsX, " + nbinsX);
        outputFile.println("underflowX, " + underflowX);
        outputFile.println("overflowX, " + overflowX);
        outputFile.println("binlowY, " + binlowY);
        outputFile.println("binwidthY, " + binwidthY);
        outputFile.println("nbinsY, " + nbinsY);
        outputFile.println("underflowY, " + underflowY);
        outputFile.println("overflowY, " + overflowY);

        // now make a loop to write the contents of each bin to disk, one number at a time
        // together with the x-coordinate of the centre of each bin.
        for (int nX = 0; nX< nbinsX; nX++) {
            // comma separated values
            for (int nY = 0; nY< nbinsY; nY++) {
            outputFile.println(nX + "," + nY + "," + binCentreX[nX] + ","
                    + binCentreY[nY] + "," + getContentX(nX) + "," + getContentX(nX)
                    + "," + getContentY(nY) + "," + getErrorX(nX)+ "," + getErrorY(nY));
            }
        }
        outputFile.close(); // close the output file
    }
}